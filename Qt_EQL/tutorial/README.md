## Info

This is a basic demo showing how to access a C++ app from Lisp, including the
creation of instances of a C++ class (the implementation of which is not really
elegant, but it works).

It also starts a simple REPL for playing around interactively.


## Build

```
qmake
make
```


## Run

```
./test
```

