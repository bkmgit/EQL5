QT           += multimedia multimediawidgets printsupport uitools
TEMPLATE     = lib
CONFIG       += no_keywords release $$(SAILFISH)
CONFIG       += plugin
INCLUDEPATH  += ../src /usr/local/include
LIBS         += -lecl -leql5 -L.. -L/usr/local/lib
TARGET       = eql5_multimedia
DESTDIR      = ../
OBJECTS_DIR  = ./tmp/multimedia/
MOC_DIR      = ./tmp/multimedia/
VERSION      = $$(EQL_VERSION)

unix {
    target.path = $$[QT_INSTALL_LIBS]
}

osx {
    target.path = /usr/local/lib
}

INSTALLS = target

win32 {
    include(windows.pri)
}

sailfish {
    # on Sailfish run this prior to run qmake:
    # $ export SAILFISH=sailfish
    QT      -= multimediawidgets printsupport uitools 
    QT      += widgets
    CONFIG  += shared_and_static build_all
    DEFINES += OS_SAILFISH
    message("*** Building for SailfishOS ***")
}

static {
    DESTDIR     = ./
    OBJECTS_DIR = ./tmp/static/multimedia/
    MOC_DIR     = ./tmp/static/multimedia/
    INSTALLS    -= target
}

HEADERS += gen/multimedia/_ini.h \
           gen/multimedia/_ini2.h \
           gen/multimedia/_q_classes.h \
           gen/multimedia/_n_classes.h \
           gen/multimedia/_q_methods.h \
           gen/multimedia/_n_methods.h

SOURCES += gen/multimedia/_ini.cpp
