QT           += webengine webenginewidgets printsupport uitools
TEMPLATE     = lib
CONFIG       += no_keywords release $$(SAILFISH)
CONFIG       += plugin
INCLUDEPATH  += ../src /usr/local/include
LIBS         += -lecl -leql5 -L.. -L/usr/local/lib
TARGET       = eql5_webengine
DESTDIR      = ../
OBJECTS_DIR  = ./tmp/webengine/
MOC_DIR      = ./tmp/webengine/
VERSION      = $$(EQL_VERSION)

unix {
    target.path = $$[QT_INSTALL_LIBS]
}

osx {
    target.path = /usr/local/lib
}

INSTALLS = target

macx:QT     += network

win32 {
    include(windows.pri)
}

sailfish {
    # on Sailfish run this prior to run qmake:
    # $ export SAILFISH=sailfish
    QT      -= printsupport uitools
    CONFIG  += shared_and_static build_all
    DEFINES += OS_SAILFISH
    message("*** Building for SailfishOS ***")
}

static {
    DESTDIR     = ./
    OBJECTS_DIR = ./tmp/static/webengine/
    MOC_DIR     = ./tmp/static/webengine/
    INSTALLS    -= target
}

HEADERS += gen/webengine/_ini.h \
           gen/webengine/_ini2.h \
           gen/webengine/_q_classes.h \
           gen/webengine/_n_classes.h \
           gen/webengine/_q_methods.h \
           gen/webengine/_n_methods.h

SOURCES += gen/webengine/_ini.cpp
