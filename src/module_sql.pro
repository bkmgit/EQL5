QT           += sql printsupport uitools
TEMPLATE     = lib
CONFIG       += no_keywords release $$(SAILFISH)
CONFIG       += plugin
INCLUDEPATH  += ../src /usr/local/include
LIBS         += -lecl -leql5 -L.. -L/usr/local/lib
TARGET       = eql5_sql
DESTDIR      = ../
OBJECTS_DIR  = ./tmp/sql/
MOC_DIR      = ./tmp/sql/
VERSION      = $$(EQL_VERSION)

unix {
    target.path = $$[QT_INSTALL_LIBS]
}

osx {
    target.path = /usr/local/lib
}

INSTALLS = target

win32 {
    include(windows.pri)
}

sailfish {
    # on Sailfish run this prior to run qmake:
    # $ export SAILFISH=sailfish
    QT      -= printsupport uitools 
    QT      += widgets
    CONFIG  += shared_and_static build_all
    DEFINES += OS_SAILFISH
    message("*** Building for SailfishOS ***")
}

static {
    DESTDIR     = ./
    OBJECTS_DIR = ./tmp/static/sql/
    MOC_DIR     = ./tmp/static/sql/
    INSTALLS    -= target
}

HEADERS += gen/sql/_ini.h \
           gen/sql/_ini2.h \
           gen/sql/_q_classes.h \
           gen/sql/_n_classes.h \
           gen/sql/_q_methods.h \
           gen/sql/_n_methods.h

SOURCES += gen/sql/_ini.cpp
