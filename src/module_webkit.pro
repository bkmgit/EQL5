QT           += webkit webkitwidgets printsupport uitools
TEMPLATE     = lib
CONFIG       += no_keywords release $$(SAILFISH)
CONFIG       += plugin
INCLUDEPATH  += ../src /usr/local/include
LIBS         += -lecl -leql5 -L.. -L/usr/local/lib
TARGET       = eql5_webkit
DESTDIR      = ../
OBJECTS_DIR  = ./tmp/webkit/
MOC_DIR      = ./tmp/webkit/
VERSION      = $$(EQL_VERSION)
macx:QT      += network

unix {
    target.path = $$[QT_INSTALL_LIBS]
}

osx {
    target.path = /usr/local/lib
}

INSTALLS = target

win32 {
    include(windows.pri)
}

sailfish {
    # on Sailfish run this prior to run qmake:
    # $ export SAILFISH=sailfish
    QT      -= printsupport uitools
    CONFIG  += shared_and_static build_all
    DEFINES += OS_SAILFISH
    message("*** Building for SailfishOS ***")
}

static {
    DESTDIR     = ./
    OBJECTS_DIR = ./tmp/static/webkit/
    MOC_DIR     = ./tmp/static/webkit/
    INSTALLS    -= target
}

HEADERS += gen/webkit/_ini.h \
           gen/webkit/_ini2.h \
           gen/webkit/_q_classes.h \
           gen/webkit/_n_classes.h \
           gen/webkit/_q_methods.h \
           gen/webkit/_n_methods.h

SOURCES += gen/webkit/_ini.cpp
