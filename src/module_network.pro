QT           += network printsupport uitools
TEMPLATE     = lib
CONFIG       += no_keywords release $$(SAILFISH)
CONFIG       += plugin
INCLUDEPATH  += ../src /usr/local/include
LIBS         += -lecl -leql5 -L.. -L/usr/local/lib
TARGET       = eql5_network
DESTDIR      = ../
OBJECTS_DIR  = ./tmp/network/
MOC_DIR      = ./tmp/network/
VERSION      = $$(EQL_VERSION)

unix {
    target.path = $$[QT_INSTALL_LIBS]
}

osx {
    target.path = /usr/local/lib
}

INSTALLS = target

win32 {
    include(windows.pri)
}

sailfish {
    # on Sailfish run this prior to run qmake:
    # $ export SAILFISH=sailfish
    QT      -= printsupport uitools 
    QT      += widgets
    CONFIG  += shared_and_static build_all
    DEFINES += OS_SAILFISH
    message("*** Building for SailfishOS ***")
}

static {
    DESTDIR     = ./
    OBJECTS_DIR = ./tmp/static/network/
    MOC_DIR     = ./tmp/static/network/
    INSTALLS    -= target
}

HEADERS += gen/network/_ini.h \
           gen/network/_ini2.h \
           gen/network/_q_classes.h \
           gen/network/_n_classes.h \
           gen/network/_q_methods.h \
           gen/network/_n_methods.h

SOURCES += gen/network/_ini.cpp
