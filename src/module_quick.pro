QT           += quick quickwidgets qml printsupport uitools
TEMPLATE     = lib
CONFIG       += no_keywords release $$(SAILFISH)
CONFIG       += plugin
INCLUDEPATH  += ../src /usr/local/include
LIBS         += -lecl -leql5 -L.. -L/usr/local/lib
TARGET       = eql5_quick
DESTDIR      = ../
OBJECTS_DIR  = ./tmp/quick/
MOC_DIR      = ./tmp/quick/
VERSION      = $$(EQL_VERSION)

unix {
    target.path = $$[QT_INSTALL_LIBS]
}

osx {
    target.path = /usr/local/lib
}

INSTALLS = target

win32 {
    include(windows.pri)
}

sailfish {
    # on Sailfish run this prior to run qmake:
    # $ export SAILFISH=sailfish
    QT      -= quickwidgets printsupport uitools 
    QT      += widgets
    CONFIG  += shared_and_static build_all
    DEFINES += OS_SAILFISH
    message("*** Building for SailfishOS ***")
}

static {
    DESTDIR     = ./
    OBJECTS_DIR = ./tmp/static/quick/
    MOC_DIR     = ./tmp/static/quick/
    INSTALLS    -= target
}

HEADERS += gen/quick/_ini.h \
           gen/quick/_ini2.h \
           gen/quick/_q_classes.h \
           gen/quick/_n_classes.h \
           gen/quick/_q_methods.h \
           gen/quick/_n_methods.h \
           gen/quick/qml_lisp.h

SOURCES += gen/quick/_ini.cpp \
           gen/quick/qml_lisp.cpp
